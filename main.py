#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# Copyright (C) 2021 WStanley <stanley_23@mail.ru>

"""
Backup script for linux server with postgresql databases
"""

import datetime
import logging
import logging.config
import os
import sys

from services.postgresql import PostgreSQLBackup
from services.service import Service
from utils.helpers import check_and_create_folder, del_old_backups
from utils.logconf import get_logging_conf
from utils.parceargs import parse_args
from utils.sendmail import send_email
from utils.setconf import SECTION_SETTINGS, parse_config

logging.config.dictConfig(get_logging_conf())
base_logger = logging.getLogger('base')


__version__ = '2.0'


CONFIG_FILE = './config.ini'
LOGS_FOLDER = './logs'


def start(dump: bool = False, dumpall: bool = False, folders_archive: bool = False,
          globals_only: bool = False, schema_only: bool = False) -> None:
    """
    dump - Создает дамп каждой БД для каждого пользователя указанного в конфиге в секции пользователя db_name
    dumpall - Создает дамп всего кластера в один файл скрипта
    folders_archive - Создает архив папок указанных в конфиге в секции пользователя backups_dirs
    globals_only - Создает дамп только глобальных объектов (ролей и табличных пространств), без баз данных.
    schema_only - Создает дамп только определения объекта (схему), но не данные.
    """

    log_path = os.path.join(os.path.dirname(os.path.realpath(__name__)), LOGS_FOLDER)

    if not os.path.exists(log_path):
        os.makedirs(log_path)

    base_logger.info('START!')

    config = parse_config(CONFIG_FILE)

    server_name = config.get('settings', 'server_name')
    backups_path = config.get('settings', 'backups_path')
    keep_backup_day = int(config.get('settings', 'keep_backup_day'))

    current_date = datetime.date.today().strftime('%Y-%m-%d')
    current_backup_path = os.path.join(backups_path, current_date)

    try:
        check_and_create_folder(current_backup_path)
    except OSError as e:
        msg = f'Cant create folder current_backup_path - {e}'
        base_logger.error(msg)
        send_email('backups error', msg, server_name)
        sys.exit()

    try:
        del_old_backups(backups_path, keep_backup_day)
    except OSError as e:
        msg = f'Cant remove folder old backups - {e}'
        base_logger.error(msg)
        send_email('backups error', msg, server_name)

    service = Service(current_backup_path)

    postgresql_backup = PostgreSQLBackup(service)

    msg_complite = 'Backups complite: '

    if dumpall:
        msg_complite += '\n- create_dumpall'
        postgresql_backup.create_dumpall()

    if globals_only:
        msg_complite += '\n- create_globals_only'
        postgresql_backup.create_globals_only()

    if schema_only:
        msg_complite += '\n- create_schema_only'
        postgresql_backup.create_schema_only()

    if folders_archive or dump:
        for section in config.sections():
            """Перебираем секции пользователей"""

            if section == SECTION_SETTINGS:
                """Исключаем секцию settings"""
                continue

            if dump:
                msg_complite += '\n- create_dump user_section: %s' % section
                db_name = config.get(section, 'db_name')
                postgresql_backup.create_dump(db_name)

            if folders_archive:
                msg_complite += '\n- folders_archive user_section: %s' % section

                user_name = config.get(section, 'user_name')
                home_path = config.get(section, 'home_path')
                backups_dirs = config.get(section, 'backups_dirs')
                exclude_dirs = config.get(section, 'exclude_dirs')

                for backup_dir in backups_dirs.split(','):

                    if os.path.isdir(backup_dir):
                        service.tar_folder(user_name, home_path, backup_dir,
                                           exclude_dirs, server_name)
                    else:
                        msg = f"backup_dir {backup_dir} is not exist, check configs - backups_dirs"
                        base_logger.error(msg)
                        send_email('backups error', msg, server_name)

    send_email('backups complite', msg_complite, server_name)

    base_logger.info('STOP!')


if __name__ == "__main__":

    args = parse_args(sys.argv[1:], __doc__, __version__)

    start(dump=args.dump, dumpall=args.dumpall, folders_archive=args.folders_archive,
          globals_only=args.globals_only, schema_only=args.schema_only)
