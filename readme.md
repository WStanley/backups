# Бекапы

## Настройка

### В корне необходимо создать файл .env с таким содержимым:
```
DB_HOST=localhost
DB_PORT=5432
POSTGRES_PASS=

SMTP_SERVER=smtp.yandex.ru
SMTP_PORT=587
SMTP_SENDER=
SMTP_LOGIN=
SMTP_PASS=
SMTP_TOADDR=  # email1,email2 or email
```

### В корне необходимо создать файл config.ini с таким содержимым:
```
# Секция с общими опциями
[settings]

# Prefix название сервера, deafault=unknow_serv
server_name: my

# Название файла логов с ошибками, deafault=base.log
log_base_name: base.log

# Название файла логов от БД, deafault=db.log
log_db_name: db.log

# Путь в домашнюю директорию пользователя root
home_path: /home/user_name

# Путь куда сохранять бекапы
backups_path: ${home_path}/yandex/backup/${server_name}

# Количество дней хранения бекапа, deafault=5
keep_backup_day: 5


# Секция пользователя
[user1]

# Имя пользователя
user_name: stanley

# Название БД для пользователя
db_name: dosug

# Путь в домашнюю директорию пользователя
home_path: /home/user_name

# Директории которые необходимо исключить из архивирования через запятую
exclude_dirs: ./git,./media/CACHE,./CACHE

# Директории кот необходимо заархивировать
backups_dirs: ${home_path}/www/path,${home_path}/www/path


# Можно добавлять несколько секций пользователей
[user2]
...

[user3]
...


```


## Установка

### Создаем папку проекта, клонируем, разварачиваем вирт окружение и активируем его

```
mkdir backup_folder_name

cd backup_folder_name

git clone https://WStanley@bitbucket.org/WStanley/backups.git .

virtualenv -p python3.8 venv

source venv/bin/activate
```

### Устанавливаем зависимости
```
pip install -r requirements.txt
```

### Можно обойтись и без виртуального окружения установив глобально python-dotenv
```
python3.8 -m pip install python-dotenv==0.19.0
```

## Установка yandex-disk

####
```
# Ссылка - https://yandex.ru/support/disk-desktop-linux/start.html

# Установка пакета
echo "deb http://repo.yandex.ru/yandex-disk/deb/ stable main" | sudo tee -a /etc/apt/sources.list.d/yandex-disk.list > /dev/null && wget http://repo.yandex.ru/yandex-disk/YANDEX-DISK-KEY.GPG -O- | sudo apt-key add - && sudo apt-get update && sudo apt-get install -y yandex-disk

# Мастер настройки
yandex-disk setup

# Команды - https://yandex.ru/support/disk-desktop-linux/cli-commands.html
```

## Запуск

#### Запуск всех функций бекапа: pg_dump, dumpall, archiving all folders, dumpall -globals_only, dumpall -schema_only
```
python main.py -a

Либо если импортировать:

from main.py import start
start(dump=True, dumpall=True, folders_archive=True, globals_only=True, schema_only=True)

```

## Запуск функций на выбор
### pg_dump всех БД перечисленных в файле env - DB_NAMES
```
python main.py -d

Либо если импортировать:

from main.py import start
start(dump=True)

```

### pg_dumpall всего кластера
```
python main.py -dll

Либо если импортировать:

from main.py import start
start(dumpall=True)

```

### Архивация папок перечисленных в файле config - backups_dirs
```
python main.py -f

Либо если импортировать:

from main.py import start
start(folders_archive=True)

```

### Дамп только глобальных объектов (роли и табличные пространства). pg_dumpall --globals_only.
```
python main.py -g

Либо если импортировать:

from main.py import start
start(globals_only=True)

```

### Дамп только схем БД. pg_dumpall -schema_only.
```
python main.py -s

Либо если импортировать:

from main.py import start
start(schema_only=True)
```

### Можно комбинировать
```
python main.py -d -f -s

Либо если импортировать:

from main.py import start
start(dump=True, folders_archive=True, schema_only=True)
```

### Пример запуска с cron
```
0 7 */2 * * cd backup_script/ && python3.8 main.py -d -g -s
0 8 */2 * * cd backup_script/ && python3.8 main.py -f
0 9 */2 * * cd backup_script/ && python3.8 main.py -dll
```

## Справка
```
python main.py -h
------------------

usage: Backups [-h] [-v] [-a] [-d] [-dll] [-f] [-g] [-s]

Backup script for linux server with postgresql databases

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -a, --all             Run all backup functions
  -d, --dump            pg_dump - for backing up a PostgreSQL database listed in env file (DB_NAMES)
  -dll, --dumpall       pg_dumpall - for writing out (“dumping”) all PostgreSQL databases of a cluster into one script file
  -f, --folders_archive Archiving all folders listed in config file (backups_dirs)
  -g, --globals_only    Dump only global objects (roles and tablespaces), no databases.
  -s, --schema_only     Dump only the object definitions (schema), not data.
```