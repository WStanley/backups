from abc import ABC, abstractmethod


class DBInterface(ABC):

    @abstractmethod
    def create_dumpall(self):
        raise NotImplementedError

    @abstractmethod
    def create_dump(self):
        raise NotImplementedError

    @abstractmethod
    def create_schema_only(self):
        raise NotImplementedError

    @abstractmethod
    def create_globals_only(self):
        raise NotImplementedError
