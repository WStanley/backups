import os

from dotenv import load_dotenv

from services.common import DBInterface
from services.service import Service

load_dotenv()


class PostgreSQLBackup(DBInterface):

    def __init__(self, service: Service) -> None:
        self.service = service
        self.host = os.getenv('DB_HOST')
        self.postgres_pass = os.getenv('POSTGRES_PASS')

    def create_dumpall(self):
        """Выгружаем все БД кластера в один файл скрипта"""
        self.service.run_command(
            f"PGPASSWORD=\'{self.postgres_pass}\' "
            f"pg_dumpall --verbose -h {self.host} -U postgres "
            f"-w > {self.service.current_backup_path}/pgdumpall.sql"
        )

    def create_dump(self, db_name: str):
        """Выгружаем одну БД"""
        self.service.run_command(
            f"PGPASSWORD=\'{self.postgres_pass}\' "
            f"pg_dump --verbose -h {self.host} -Ft -U postgres "
            f"-w {db_name} > {self.service.current_backup_path}/{db_name}_pgdump.tar"
        )

    def create_schema_only(self) -> None:
        """Выгрузить только определения объекта (схему), но не данные."""
        self.service.run_command(
            f"PGPASSWORD=\'{self.postgres_pass}\' "
            f"pg_dumpall --verbose -h {self.host} -U postgres "
            f"--clean --schema-only -w > {self.service.current_backup_path}/schema_only_pgdumpall.sql"
        )

    def create_globals_only(self):
        """Дамп только глобальных объектов (ролей и табличных пространств), без баз данных."""
        self.service.run_command(
            f"PGPASSWORD=\'{self.postgres_pass}\' "
            f"pg_dumpall --verbose -h {self.host} -U postgres "
            f"--clean --globals-only -w > {self.service.current_backup_path}/globals_only_pgdumpall.sql"
        )
