import logging
import logging.config
import os
import subprocess
import tarfile

from utils.logconf import get_logging_conf
from utils.sendmail import send_email

logging.config.dictConfig(get_logging_conf())


class Service():

    def __init__(self, current_backup_path) -> None:
        self.current_backup_path = current_backup_path
        self.base_logger = logging.getLogger('base')
        self.db_logger = logging.getLogger('db')

    def run_command(self, command: str) -> None:
        """Запускаем команду"""
        run_command = subprocess.run(
            command,
            shell=True,
            stderr=subprocess.PIPE,
            encoding='utf-8'
        )
        # Если returncode == 0 значит бекап завершился с ошибкой
        if run_command.returncode == 0:
            self.db_logger.info(run_command.stderr)
        else:
            self.base_logger.error(run_command.stderr)
            send_email('backups error', run_command.stderr)

    def tar_folder(self, user_name: str, home_path: str, backup_dir: str,
                   exclude_dirs: str, server_name: str) -> None:
        os.chdir(backup_dir)

        filename = f"{os.path.basename(backup_dir)}_{user_name}.tar.gz"
        tar_file_name = (
            f"{self.current_backup_path}/{filename}"
        )

        def filter_dirs(tarinfo):
            if tarinfo.name in exclude_dirs.split(','):
                return None
            return tarinfo

        try:
            with tarfile.open(tar_file_name, 'w:gz') as tar:
                tar.add('.', filter=filter_dirs)
        except OSError as e:
            msg = f"archive not created {e}"
            self.base_logger.error(msg)
            send_email('backups error', msg, server_name)

        os.chdir(home_path)
