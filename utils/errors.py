class ConfigFileError(Exception):
    """Отсутствует файл конфигурации"""
    pass


class ConfigSectionError(Exception):
    """Нет секции в файле конфига"""
    pass


class ConfigOptionError(Exception):
    """Отсутствует необходимая настройка"""
    pass
