import datetime
import logging
import logging.config
import os
import shutil

from utils.logconf import get_logging_conf

logging.config.dictConfig(get_logging_conf())
base_logger = logging.getLogger('base')


def check_and_create_folder(path: str) -> None:
    """Создаем папку если она не существует"""
    if not os.path.exists(path):
        os.makedirs(path)
        base_logger.info('Create folder %s' % path)


def del_old_backups(backups_path: str, keep_backup_day: int) -> None:
    """Удаляем старые бекапы, кот более keep_backup_day дней"""
    for folder_name in os.listdir(backups_path):
        folder_create = datetime.datetime.strptime(folder_name, "%Y-%m-%d").date()

        # Если дата папки превышает текущую дату на keep_backup_day дней то удаляем ее
        if datetime.datetime.now().date() - folder_create > datetime.timedelta(days=keep_backup_day):
            shutil.rmtree(os.path.join(backups_path, folder_name))
            base_logger.info('Remove folder %s' % folder_name)
