# from logging.handlers
def get_logging_conf():
    return {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': '{levelname} APP:{name}; {asctime} {module} {process:d} {thread:d}  LINE:{lineno} {message}',
                'style': '{',
            },
            'simple': {
                'format': '{levelname} {message}',
                'style': '{',
            },
        },
        'handlers': {
            'console': {
                'level': 'ERROR',
                'class': 'logging.StreamHandler',
                'formatter': 'simple'
            },
            'base_log': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'verbose',
                'filename': 'logs/base.log'
            },
            'db_log': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'verbose',
                'filename': 'logs/db.log'
            },
        },
        'loggers': {
            'base': {
                'handlers': ['base_log'],
                'level': 'INFO',
            },
            'db': {
                'handlers': ['db_log'],
                'level': 'INFO',
            },
        }
    }
