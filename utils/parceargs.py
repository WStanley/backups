import argparse


def create_parser(doc: str = '', version: str = '') -> argparse.ArgumentParser:
    """Создание парсера командной строки"""

    parser = argparse.ArgumentParser(description=doc, prog='Backups')

    parser.add_argument('-v', '--version', action='version', version=version)

    parser.add_argument('-a', '--all', action='store_true', default=False,
                        help='Run all backup functions')

    parser.add_argument('-d', '--dump', action='store_true', default=False,
                        help="""pg_dump - for backing up a PostgreSQL database
                            listed in env file (DB_NAMES)""")

    parser.add_argument('-dll', '--dumpall', action='store_true', default=False,
                        help="""pg_dumpall - for writing out (“dumping”) all
                            PostgreSQL databases of a cluster into one script file""")

    parser.add_argument('-f', '--folders_archive', action='store_true', default=False,
                        help='Archiving all folders listed in config file (backups_dirs)')

    parser.add_argument('-g', '--globals_only', action='store_true', default=False,
                        help='Dump only global objects (roles and tablespaces), no databases.')

    parser.add_argument('-s', '--schema_only', action='store_true', default=False,
                        help='Dump only the object definitions (schema), not data.')

    return parser


def parse_args(arguments: list, doc: str = '', version: str = '') -> argparse.Namespace:
    """Парсим опции командной строки"""

    parser = create_parser(doc, version)
    args = parser.parse_args(arguments)

    if args.all:
        args.dump = True
        args.dumpall = True
        args.folders_archive = True
        args.globals_only = True
        args.schema_only = True

    return args
