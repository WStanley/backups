import os
import smtplib
from dotenv import load_dotenv

load_dotenv()


def send_email(subject: str, message: str,
               server_name: str = '') -> None:

    body_msg = '\r\n'.join((
        f'From: {os.getenv("SMTP_SENDER")}',
        f'To: {os.getenv("SMTP_TOADDR")}',
        f"Subject: {server_name} {subject}",
        '',
        message,
    ))
    server = smtplib.SMTP(os.getenv('SMTP_SERVER'), os.getenv('SMTP_PORT'))
    server.ehlo()
    server.starttls()
    server.login(os.getenv('SMTP_LOGIN'), os.getenv('SMTP_PASS'))
    server.sendmail(
        os.getenv('SMTP_SENDER'),
        os.getenv('SMTP_TOADDR').split(','),
        body_msg.encode('utf-8')
    )
    server.quit()
