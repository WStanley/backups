import logging
import logging.config
import os
from configparser import ConfigParser, ExtendedInterpolation

from utils.errors import ConfigFileError, ConfigOptionError, ConfigSectionError
from utils.logconf import get_logging_conf

logging.config.dictConfig(get_logging_conf())
base_logger = logging.getLogger('base')


SECTION_SETTINGS = 'settings'
SECTION_SETTINGS_OPTIONS = (
    ('server_name', 'unknow_serv'),
    ('log_base_name', 'base.log'),
    ('log_db_name', 'db.log'),
    ('home_path', None),
    ('backups_path', None),
    ('keep_backup_day', '5'),
)

SECTION_USER_OPTIONS = (
    'user_name',
    'db_name',
    'home_path',
    'exclude_dirs',
    'backups_dirs',
)


def set_options_settings(parser: ConfigParser) -> None:
    """Проверяем основную секцию с опциями, назначаем значения по дефолту"""
    for opt, default in SECTION_SETTINGS_OPTIONS:

        if opt not in parser.options(SECTION_SETTINGS) and not default:
            msg = 'Configuration section setting option missing - %s' % opt
            base_logger.error(msg)
            raise ConfigOptionError(msg)

        if opt not in parser.options(SECTION_SETTINGS):
            parser[SECTION_SETTINGS][opt] = default


def check_options_user(parser: ConfigParser, section: str) -> None:
    """Проверяем наличие всех опций в секции пользователя"""
    for opt in SECTION_USER_OPTIONS:

        if opt not in parser.options(section):
            msg = 'Configuration user section %s option missing - %s' % (section, opt)
            base_logger.error(msg)
            raise ConfigOptionError(msg)


def parse_config(filename: str = None) -> ConfigParser:

    filepath = os.path.abspath(filename)

    if not os.path.isfile(filepath):
        msg = 'Configuration file %s does not exist.' % filepath
        base_logger.error(msg)
        raise ConfigFileError(msg)

    parser = ConfigParser(interpolation=ExtendedInterpolation())
    parser.read(filepath)

    if SECTION_SETTINGS not in parser:
        msg = 'Configuration section missing - %s' % SECTION_SETTINGS
        base_logger.error(msg)
        raise ConfigSectionError(msg)

    set_options_settings(parser)

    for section in parser.sections():
        if section == SECTION_SETTINGS:
            continue
        check_options_user(parser, section)

    if not parser.get(SECTION_SETTINGS, 'keep_backup_day').isdigit():
        msg = 'Configuration section setting option keep_backup_day must be integer'
        base_logger.error(msg)
        raise ConfigOptionError(msg)

    return parser
